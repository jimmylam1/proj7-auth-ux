import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import os
from pymongo import MongoClient
from flask_restful import Resource, Api
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
from forms import RegisterForm, LoginForm, TokenAuthForm
from password import hash_password, verify_password
from login_tokens import generate_auth_token, verify_auth_token
import logging

###
# Globals
###
app = flask.Flask(__name__)
app.secret_key = 'vadfvaegaetfbv'

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevtimes

###
# RESTful API
###
api = Api(app)

def sortTimes(times):
    """ Sort the times in ascending order. I need this function, instead
        of just using the list .sort() method, because it does not correctly
        sort the hours (10:00 sorts before 1:00). The times list from the
        argument is sorted as arrow objects since this list can be sorted
        correctly. Use the original times list instead of the sorted
        arrow list since the arrow list contains time offsets.
    """
    unsorted_times = [arrow.get(i, "ddd M/D H:mm") for i in times]
    sorted_times = [arrow.get(i, "ddd M/D H:mm") for i in times]
    sorted_times.sort()

    new_times = []
    for i in range(len(sorted_times)):
        for j in range(len(unsorted_times)):
            if sorted_times[i] == unsorted_times[j]:
                new_times.append(times[j])
                break
    return new_times

@login_required
def getTimes(time_type, top=None):
    """ Get the open or close time from the database.

    Args:
        time_type: (either 'open' or 'close') The field
                   to search for in the database.
        top: return only the "top k" times

    Returns:
        A list of either open or close times.

    # check for token
    t = request.headers.get('Authorization')
    if not t:
        app.logger.debug('Did not find token')
        return None
    if not verify_auth_token(t):
        app.logger.debug('Token verification failed')
        return None
    """

    raw_data = db.brevtimes.find()
    times = []
    for location in raw_data:
        if time_type in location:
            # location is essentially a dictionary.
            # location['open'] would get just the open time, similar for the close time
            times.append(location[time_type])

    times = sortTimes(times)

    if top is not None:
        app.logger.debug("getTimes(): top = {}".format(top))
        times = times[:top]
        app.logger.debug("new times list: {}".format(times))
    return times

class ListAllJSON(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        open_times = getTimes('open', top)
        close_times = getTimes('close', top)
        return {'open': open_times, 'close': close_times}

class ListOpenOnlyJSON(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        return {'open': getTimes('open', top)}

class ListCloseOnlyJSON(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        return {'close': getTimes('close', top)}

class ListAllCSV(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        times = getTimes('open', top)
        times += getTimes('close', top)
        response = flask.make_response(",".join(times))
        return response

class ListOpenOnlyCSV(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        times = getTimes('open', top)
        response = flask.make_response(",".join(times))
        return response

class ListCloseOnlyCSV(Resource):
    def get(self):
        top = request.args.get('top', type=int)
        times = getTimes('close', top)
        response = flask.make_response(",".join(times))
        return response

api.add_resource(ListAllJSON, '/listAll', '/listAll/json')
api.add_resource(ListOpenOnlyJSON, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(ListCloseOnlyJSON, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(ListAllCSV, '/listAll/csv')
api.add_resource(ListOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(ListCloseOnlyCSV, '/listCloseOnly/csv')

###
# registration
###
UNIQUE_ID = 1

login_manager = LoginManager()
login_manager.setup_app(app)

USERS = {}
USER_NAMES = {}

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

class UserResource(Resource):
    def get(self, _user):
        # return JSON of user in DB
        return current_user

def update_users():
    """ Updates the USERS with names from the DB """
    global USERS, USER_NAMES
    _items = db.brevtimes.find()
    users_list = [item for item in _items if 'username' in item]
    # return User class if found in DB, None otherwise
    for user in users_list:
        name = u"{}".format(user['username'])
        user_id = int(user['id'])
        USERS[user_id] = User(name, user_id)
        USER_NAMES[name] = User(name, user_id)

@login_manager.user_loader
def load_user(cur_id):
    cur_id = int(cur_id)
    return USERS.get(cur_id)

@login_manager.request_loader
def load_user_request(request):
    user_id = request.args.get('api-key')
    if user_id is None:
        user_id = request.headers.get('Authorization')

    return USERS.get(user_id)

@app.route("/register", methods=['GET', 'POST'])
def register():
    global UNIQUE_ID

    if request.method == 'GET':
        return flask.Response(status=400)

    form = RegisterForm()
    if form.validate_on_submit():
        # get values from the form
        username = form.username.data
        password = form.password.data
        hashed_pwd = hash_password(password)

        # store into database
        item_doc = {'id': UNIQUE_ID,
                    'username': username,
                    'password': hashed_pwd
                    }
        db.brevtimes.insert_one(item_doc)
        UNIQUE_ID += 1

        update_users()  # update USERS and USER_NAMES

        result = {'username': username}
        response = flask.make_response(flask.jsonify(result=result), 201)
        response.headers['location'] = username

        #user = USER_NAMES[form.username.data]
        #login_user(user)
        #return flask.redirect(flask.url_for('index'))
        return response

    return flask.render_template('register.html', title='Sign In', form=form)

@app.route('/token', methods=['GET'])
@login_required
def get_token():
    duration = 600
    t = generate_auth_token(duration)
    result = {'token': t.decode(),
              'duration': duration
              }
    return flask.jsonify(result=result)

def getPassword(username):
    _items = db.brevtimes.find()
    if username in USER_NAMES:
        for item in _items:
            if 'username' in item and item['username'] == username:
                return str(item['password'])
        return hash_password('FAILED')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return flask.render_template('calc.html', user_msg="Already logged in. Please logout to switch accounts.")
    form = LoginForm()
    if form.validate_on_submit():
        remember = form.remember_me.data
        username = form.username.data
        password = form.password.data
        if username in USER_NAMES:
            if verify_password(password, getPassword(username)):
                user = USER_NAMES[form.username.data]
                login_user(user, remember=remember)
                return flask.redirect(flask.url_for('index'))
        flask.flash("Error logging in. Make sure your password is correct!")
        return flask.redirect(flask.url_for('login'))
    return flask.render_template('login.html', title='Sign In', form=form)

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    #return flask.redirect(flask.url_for('index'))
    return flask.render_template('calc.html', user_msg="Logged out")


###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    # clear collection
    # db.brevtimes.drop()
    try:
        user_msg = "Welcome, {}".format(current_user.name)
    except:
        user_msg = 'Not logged in'

    return flask.render_template('calc.html', user_msg=user_msg)

@app.route("/_times")
def _times():
    return flask.render_template('times.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found: {}".format(error))
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    '''
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    '''
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', type=int)
    date = request.args.get('date', type=str)
    time = request.args.get('time', type=str)

    # Example: date='2017-01-01' time='00:00'
    date_and_time = arrow.get('{} {}'.format(date, time), 'YYYY-MM-DD H:mm')

    app.logger.debug("km={}".format(km))

    open_time = acp_times.open_time(km, dist, date_and_time)
    close_time = acp_times.close_time(km, dist, date_and_time)
    app.logger.debug("open: {} close: {}".format(open_time, close_time))

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

def errorCheck(miles, kms):
    """ Auxiliary function for submit button. This ensures that the entries
        are not empty, and it makes sure the entries do not contain a letter
        or space.
    """
    letters = " abscdfghijklmnopqrstuvwxyz"

    distances = [miles, kms]
    empty = 1

    # check for characters
    for distance in distances:
        for point in distance:
            if point != '':
                empty = 0
                point = point.lower()
                for letter in letters:
                    if letter in point:
                        return -1  # found a letter in either km or miles
    if empty:
        return 0  # km or miles are empty
    return 1  # all good, should save into database

@app.route('/_sub', methods=['POST', 'GET'])
def _submit():
    """ Submit button was pressed """
    app.logger.debug("Submit button was pressed")

    distance = request.args.get('distance', type=int)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)

    miles = request.args.getlist('miles', type=str)
    kms = request.args.getlist('km', type=str)
    locations = request.args.getlist('location', type=str)
    open_times = request.args.getlist('open', type=str)
    close_times = request.args.getlist('close', type=str)

    # check for errors in the km or miles lists
    err_val = errorCheck(miles, kms)
    if err_val == -1:
        msg = 'ERROR: You can only enter numbers for the controle distances!'
    elif err_val == 0:
        msg = 'ERROR: You must enter at least one controle distance before submitting'
    else:
        msg = 'Entries were successfully entered into the database'

        # clear collection
        db.brevtimes.drop()

        # add brevet info into database
        item_doc = {'distance': distance,
                    'begin_date': begin_date,
                    'begin_time': begin_time}
        db.brevtimes.insert_one(item_doc)

        # add the rest
        for i in range(len(miles)):
            if kms[i] != '':
                item_doc = {
                    "mile": miles[i],
                    "km": kms[i],
                    "location": locations[i],
                    "open": open_times[i],
                    "close": close_times[i]
                }
                db.brevtimes.insert_one(item_doc)

    result = {'msg': msg}
    return flask.jsonify(result=result)

@app.route('/_display', methods=['POST'])
def _display():
    """ display button pressed """
    app.logger.debug("Display button was pressed")
    msg = ""
    _items = db.brevtimes.find()
    items = []  # store the times
    info = {'distance': "", 'begin_date': "", "begin_time": ""}  # default

    for item in _items:
        if 'distance' in item:
            info = item  # get the brevet info (distance, start date and time)
        else:
            items.append(item)
    app.logger.debug("found in DB: {}".format(items))

    if len(items) == 0:
        msg = "ERROR: Nothing found from the database"

    err_msg = {"msg": msg}
    return flask.render_template('times.html', items=items, info=info, msg=err_msg)

#############

app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
