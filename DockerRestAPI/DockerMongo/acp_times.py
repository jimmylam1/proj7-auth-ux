"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.

    """
    distances = [0, 200, 300, 400, 600, 1000, 1300]
    speeds = [0, 34, 32, 32, 30, 28, 26]
    values = [0, 200/34, 100/32, 100/32, 200/30, 400/28]

    total_time = 0

    # check for final control point
    if control_dist_km > brevet_dist_km:
        idx = distances.index(brevet_dist_km)
        for i in range(idx):
            total_time += values[i]
        total_time += values[idx]
    else:
        for i in range(1, 6):
            # find the range where the control_dist fits
            if control_dist_km > distances[i - 1] and control_dist_km <= distances[i]:
                for j in range(i):  # add up the times from the previous ranges
                    total_time += values[j]
                total_time += (control_dist_km - distances[i - 1]) / speeds[i]

    hours = int(total_time // 1)
    minutes = round((total_time - hours) * 60)

    return brevet_start_time.shift(hours=hours, minutes=minutes).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600, or 1000
           (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    distances = [0, 200, 300, 400, 600, 1000, 1300]
    speeds = [0, 15, 15, 15, 15, 11.428, 13.333]
    values = [0, 200/15, 100/15, 100/15, 200/15, 200/11.482, 400/13.333]
    time_limits = [0, 13.5, 20, 27, 40, 75]  # in hours

    total_time = 0

    # oddity: distance less than 60km have a relaxed time as 1 hr + dist/20
    if control_dist_km < 60:
        total_time = 1 + (control_dist_km / 20)
    elif control_dist_km >= brevet_dist_km:
        idx = distances.index(brevet_dist_km)
        total_time = time_limits[idx]
    else:
        for i in range(1, len(distances) - 1):
            # find the range where the control_dist fits
            if control_dist_km > distances[i - 1] and control_dist_km <= distances[i]:
                for j in range(i):  # add up the times from the previous ranges
                    total_time += values[j]
                total_time += (control_dist_km - distances[i - 1]) / speeds[i]

    hours = int(total_time // 1)
    minutes = round((total_time - hours) * 60)

    return brevet_start_time.shift(hours=hours, minutes=minutes).isoformat()
